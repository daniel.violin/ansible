# Treinamento Descomplicando o Ansible

Projeto do treinamento para instalação de um cluster Kubernetes utilizando o Ansible + AWS.
Projeto com contribuição dos alunos.

## Fases do projeto
```
 - Provisioning > Criar as instancias/vms para o cluster.
 - Install K8S > Criação do cluster, etc...
 - Deploy App > Deploy de uma aplicação exemplo.
 - Extra > Segredo.
```


## License
[MIT](https://choosealicense.com/licenses/mit/)
